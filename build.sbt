organization := "com.agilogy"

name := "kata"

version := "0.0.1"

scalaVersion := "2.9.1"

libraryDependencies += "org.scalatest" %% "scalatest" % "1.7.1" % "test"

libraryDependencies += "org.mockito" % "mockito-all" % "1.9.0" % "test"


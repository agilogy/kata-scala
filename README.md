#Kata-scala

This is an empty repository for code katas in scala using mockito and scalatest

Before you start the kata you have to clone the repo:

	git clone https://bitbucket.org/agilogy/kata-scala.git <<local-name>>
	cd <<local-name>>

If you want to use IntelliJ IDEA the plugin is already configured in project/plugins.sbt and all you have to do is:

	sbt gen-idea 

In order to execute the tests in autotest mode you have to do:

	sbt
	~ test

Finally, we have not included a src/main/scala folder because it is not needed for a code kata (you can have all the code in the same) but if you want to separate test and "production" code all you have to do is create the folder yourself and put your scala files there.